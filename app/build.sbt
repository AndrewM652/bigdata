name := """app"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs
)


libraryDependencies += "org.mongodb" % "mongo-java-driver" % "3.0.1"
libraryDependencies += "org.mongodb.morphia" % "morphia-validation" % "1.0.0-rc0"
libraryDependencies += "org.mongodb.mongo-hadoop" % "mongo-hadoop-core" % "1.3.2"
libraryDependencies +=  "com.squareup.retrofit" % "retrofit" % "2.0.0-beta2"
libraryDependencies +=  "com.squareup.retrofit" % "converter-jackson" % "2.0.0-beta2"
libraryDependencies += "junit" % "junit" % "4.11"


// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator
