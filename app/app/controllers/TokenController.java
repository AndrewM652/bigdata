package controllers;

import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import rest.FacebookLoginHelper;

/**
 * Created by artem on 07.11.15
 */
public class TokenController extends Controller {

    public Result getToken() {
        FacebookLoginHelper flh = new FacebookLoginHelper();
        String accessToken = flh.getUserToken();
        if (accessToken == null)
            return badRequest("access_token == null");
        else
            return ok(accessToken);
    }

    public Result setToken() {
        FacebookLoginHelper flh = new FacebookLoginHelper();
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        String accessToken = dynamicForm.get("access_token");
        flh.setUserToken(accessToken);
        if (flh.getUserToken() == null)
            return badRequest("access_token == null");
        else
            return ok(accessToken);
    }

}
