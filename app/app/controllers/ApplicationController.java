package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import hadoop.MapReduceAgregate;
import hadoop.MapReduceWords;
import helpers.AnnotationInvocationHandler;
import model.*;
import model.FbSearchResponse;
import model.FbType;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.annotations.Property;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import rest.*;
import rest.IFacebookService;
import rest.RestClient;
import retrofit.Call;
import retrofit.Response;
import views.html.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class ApplicationController extends Controller {

    public Result index() {
        return ok(explore.render());
    }

    public Result explore() {
        return ok(explore.render());
    }

    public Result searchParameters(String q, String fields, Boolean isDebugMode) throws IOException {
        IFacebookService api = RestClient.getFacebookService();
        Call<FbSearchResponse> call = api.search(q, FbType.PAGE, fields);

        try {
            FbSearchResponse result = request(call);
            Debug.checkboxdebug = isDebugMode;
            ObjectMapper mapper = new ObjectMapper();

            String jsonstr = mapper.writeValueAsString(result);
            int a = jsonstr.indexOf("[");
            int b = jsonstr.lastIndexOf("]");

            jsonstr = jsonstr.substring(a+1, b);
            jsonstr = jsonstr.replace("\\"+"\"", "");

            return ok(search.render(result, "{\"0\":" + jsonstr + "}", Debug.checkboxdebug));
        } catch (IOException e) {
            return badRequest(index.render("error :( "));
        }
    }

    public Result searchPaging() throws IOException {
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        String page = dynamicForm.get("page");
        IFacebookService api = RestClient.getFacebookService();
        Call<FbSearchResponse> call = api.search(URLDecoder.decode(page));

        try {
            FbSearchResponse result = request(call);
            ObjectMapper mapper = new ObjectMapper();
            String jsonstr = mapper.writeValueAsString(result);

            int a = jsonstr.indexOf("[");
            int b = jsonstr.lastIndexOf("]");

            jsonstr = jsonstr.substring(a+1, b);
            jsonstr = jsonstr.replace("\\"+"\"", "");

            return ok(search.render(result, "{\"0\":" + jsonstr + "}", Debug.checkboxdebug));
        } catch (IOException e) {
            return badRequest(index.render("error :( "));
        }
    }

    private FbSearchResponse request(Call<FbSearchResponse> call) throws IOException {
        Response<FbSearchResponse> response = call.execute();
        if (!response.isSuccess())
            throw new IOException();
        FbSearchResponse result = response.body();
        if (result != null && result.paging != null) {
            if (result.paging.next != null)
                result.paging.next = URLEncoder.encode(result.paging.next);
            if (result.paging.previous != null)
                result.paging.previous = URLEncoder.encode(result.paging.previous);
        }
        return result;
    }

    public Result put() {
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        String q = dynamicForm.get("q");
        String fields = dynamicForm.get("fields");
        String count = dynamicForm.get("count");
        Integer counter = 0;
        Integer dataCount = 0;

        try {
            dataCount = Integer.valueOf(count);
        } catch (Exception e){
            return badRequest(put.render("Count must be number."));
        }
        if (dataCount <= 0){
            return badRequest(put.render("Count must be more than 0."));
        }


        IFacebookService api = RestClient.getFacebookService();
        Call<FbSearchResponse> call =
                api.search(
                        q,
                        FbType.PAGE,
                        fields
                );
        Response<FbSearchResponse> response;
        FbSearchResponse result;

        MongoClientSingleton mongoClientSingleton = MongoClientSingleton.getInstance();
        MongoClient client = mongoClientSingleton.client;
        Datastore ds = new Morphia().createDatastore(client, "facebook");

        ds.ensureIndexes();
        ds.ensureCaps();

        do{
            try {
                response = call.execute();
                if (!response.isSuccess())
                    throw new IOException();
                result = response.body();
                ds.save(result.data);
                counter += result.data.length;
                if (result.paging != null && result.paging.next != null)
                    call = api.search(result.paging.next);
            } catch (IOException e) {
                return badRequest(index.render("error :( \n <p>Loaded "+counter.toString()+" elements"));
            }
        } while (counter < dataCount && result.paging != null && result.paging.next != null);

        return ok(index.render("Elements loaded: "+counter.toString()));
    }

    public Result putForm(){
        return ok(put.render(" "));
    }

    public Result analyze() {
        MongoClientSingleton mongoClientSingleton = MongoClientSingleton.getInstance();
        MongoClient client = mongoClientSingleton.client;
        Datastore ds = new Morphia().createDatastore(client, "facebook");

        ds.ensureIndexes();
        ds.ensureCaps();

        long count = ds.getCount(FbPage.class);

        return ok(analyze.render(count));
    }

    public Result dbPaging() {
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        String page = dynamicForm.get("page");
        String[] pageIndexes = page.split(",");

        try {
            FbDbQuery result = getQueryResult(pageIndexes[0], pageIndexes[1], pageIndexes[2], pageIndexes[3]);
            ObjectMapper mapper = new ObjectMapper();
            String jsonstr = mapper.writeValueAsString(result);

            int a = jsonstr.indexOf("[");
            int b = jsonstr.lastIndexOf("]");

            jsonstr = jsonstr.substring(a+1, b);
            jsonstr = jsonstr.replace("\\"+"\"", "");

            return ok(dbexplore.render(result, "{\"0\":" + jsonstr + "}", Debug.checkboxdebug));
        } catch (IOException e) {
            return badRequest(index.render("error :( "));
        }
    }

    private static final int MAX_ROWS = 10;

    private FbDbQuery getQueryResult(String from, String to, String globalFrom, String globalTo) {
        int lowerBound;
        int upperBound;
        int globalLowerBound;
        int globalUpperBound;
        try {
            lowerBound = Integer.parseInt(from);
            upperBound = Integer.parseInt(to);
            globalLowerBound = Integer.parseInt(globalFrom);
            globalUpperBound = Integer.parseInt(globalTo);
        } catch (NumberFormatException e) {
            throw e;
        }

        MongoClientSingleton mongoClientSingleton = MongoClientSingleton.getInstance();
        MongoClient client = mongoClientSingleton.client;
        Datastore ds = new Morphia().createDatastore(client, "facebook");

        ds.ensureIndexes();
        ds.ensureCaps();

        long countInDb = ds.getCount(FbPage.class);

        if (globalUpperBound > countInDb) {
            globalUpperBound = (int) countInDb;
        }
        if (upperBound > countInDb) {
            upperBound = (int) countInDb;
        }

        FbDbQuery dbQuery = new FbDbQuery();
        if (globalUpperBound - globalLowerBound <= MAX_ROWS) {
            dbQuery.data = new FbPage[globalUpperBound - globalLowerBound];
            int i = 0;
            for (FbPage fbPage : ds.find(FbPage.class).offset(globalLowerBound).limit(globalUpperBound - globalLowerBound)) {
                dbQuery.data[i++] = fbPage;
            }
            dbQuery.paging = null;
        } else {
            if (upperBound - lowerBound > MAX_ROWS) {
                upperBound = lowerBound + MAX_ROWS;
            }
            dbQuery.data = new FbPage[upperBound - lowerBound];
            int i = 0;
            for (FbPage fbPage : ds.find(FbPage.class).offset(lowerBound).limit(upperBound - lowerBound)) {
                dbQuery.data[i++] = fbPage;
            }

            int nextLower = lowerBound + MAX_ROWS;
            int nextUpper = upperBound + MAX_ROWS;
            String next;

            if (nextLower >= globalUpperBound) {
                next = null;
            } else {
                StringBuilder sbNext = new StringBuilder();
                sbNext.append(nextLower).append(",");
                if (nextUpper >= globalUpperBound) {
                    nextUpper = globalUpperBound;
                }
                sbNext.append(nextUpper).append(",").append(globalFrom).append(",").append(globalTo);
                next = sbNext.toString();
            }

            int prevLower = lowerBound - MAX_ROWS;
            int prevUpper = upperBound - MAX_ROWS;
            String prev;

            if (prevUpper <= globalLowerBound) {
                prev = null;
            } else {
                StringBuilder sbPrev = new StringBuilder();
                sbPrev.append(prevLower).append(",");
                if (prevLower <= globalLowerBound) {
                    prevLower = globalLowerBound;
                }
                if (upperBound == globalUpperBound) {
                    prevUpper = prevLower + MAX_ROWS;
                }
                sbPrev.append(prevUpper).append(",").append(globalFrom).append(",").append(globalTo);
                prev = sbPrev.toString();
            }

            dbQuery.paging = new FbDbPaging(next, prev);
        }

        return dbQuery;
    }

    private FbDbResult getReduceResult() {
        MongoClientSingleton mongoClientSingleton = MongoClientSingleton.getInstance();
        MongoClient client = mongoClientSingleton.client;
        Datastore ds = new Morphia().createDatastore(client, "facebook");

        ds.ensureIndexes();
        ds.ensureCaps();

        FbDbResult dbQuery = new FbDbResult();
        List<FbDbResultItem> items = new ArrayList<>();
        for (FbDbResultItem item : ds.find(FbDbResultItem.class)) {
            items.add(item);
        }
        FbDbResultItem[] data = new FbDbResultItem[items.size()];
        items.toArray(data);
        dbQuery.data = data;
        dbQuery.count = items.size();
        return dbQuery;
    }

    public Result dbExplore(String from, String to, Boolean isDebugMode)  {
        int lowerBound = Integer.parseInt(from);
        int upperBound = Integer.parseInt(to);
        if (lowerBound >= upperBound) {
            return badRequest(index.render("Lower bound higher or equal upper bound!"));
        }
        MongoClientSingleton mongoClientSingleton = MongoClientSingleton.getInstance();
        MongoClient client = mongoClientSingleton.client;
        Datastore ds = new Morphia().createDatastore(client, "facebook");

        ds.ensureIndexes();
        ds.ensureCaps();

        Debug.checkboxdebug = isDebugMode;
        long count = ds.getCount(FbPage.class);
        if (upperBound > count) {
            return badRequest(index.render("Upper bound more than number of records in DB!"));
        }
        if (lowerBound > count) {
            return badRequest(index.render("Lower bound more than number of records in DB!"));
        }
        try {
            FbDbQuery result = getQueryResult(from, to, from, to);
            ObjectMapper mapper = new ObjectMapper();
            String jsonstr = mapper.writeValueAsString(result);

            int a = jsonstr.indexOf("[");
            int b = jsonstr.lastIndexOf("]");

            jsonstr = jsonstr.substring(a+1, b);
            jsonstr = jsonstr.replace("\\"+"\"", "");

            return ok(dbexplore.render(result, "{\"0\":" + jsonstr + "}", isDebugMode));
        } catch (IOException e) {
            return badRequest(index.render("error :( "));
        }
    }

    public Result clear() {
        MongoClientSingleton mongoClientSingleton = MongoClientSingleton.getInstance();
        MongoClient client = mongoClientSingleton.client;
        Datastore ds = new Morphia().createDatastore(client, "facebook");

        ds.delete(ds.createQuery(FbDbResultItem.class));
        ds.delete(ds.createQuery(FbPage.class));

        long count = ds.getCount(FbPage.class);

        return ok(analyze.render(count));
    }

    public Result mapReduce(String obj, String type, Integer Min) {
        String[] args={};
        try {

                    MapReduceAgregate.RunJob(args, obj, Min);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Field field = FbDbResultItem.class.getDeclaredField("category");
            Property property = field.getAnnotation(Property.class);
            AnnotationInvocationHandler.changeAnnotationValue(property, "value", obj);
        } catch (Exception e) {
            return badRequest(index.render("reflection error :( "));
        }

        switch (type) {
            case "pie":
                return ok(pie.render(getReduceResult()));
            case "geo":
                return ok(geo.render(getReduceResult()));
            case "cloud":
                FbDbResult result = getReduceResult();
                return ok(cloud.render(result));
            default:
                return ok(graph.render(getReduceResult()));
        }
    }

    public Result userinfo() {
        return ok(userinfo.render());
    }

    public Result cloud() {
        return ok(cloud.render(getReduceResult()));
    }

}