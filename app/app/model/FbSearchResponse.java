package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by artem on 30.10.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FbSearchResponse {
    public FbPage[] data;
    public FbPaging paging;
}
