package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

/**
 * Created by artem on 30.10.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(value = "pages", noClassnameStored = true)
public class FbPage {
    @Id public String id;
    public String name;
    public String birthday;
    public String bio;
    public String category;
    public String description;
    public String hometown;
    public String influences;

    @Property("talking_about_count")
    @JsonProperty("talking_about_count")
    public int talkingAbout;
    public int likes;

    @Embedded public FbLocation location;
}
