package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by volk on 05.11.15.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FbPaging {
    public String next;
    public String previous;
}
