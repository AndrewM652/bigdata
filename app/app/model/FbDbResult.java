package model;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

/**
 * Created by volk on 04.12.15.
 */
@Entity(value = "results", noClassnameStored = true)
public class FbDbResult {
    @Embedded
    public FbDbResultItem data[];
    public Integer count;
}
