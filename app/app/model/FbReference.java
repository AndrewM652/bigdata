package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Created by andrewm on 02.11.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class FbReference {
    @Id public String id;
    public String name;

    @Override
    public String toString() {
        return name;
    }
}
