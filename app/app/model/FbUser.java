package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.List;

/**
 * Created by artem on 30.10.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class FbUser {
    @Id public String id;
    public String name;
    public String birthday;
    public String gender;
    public String first_name;
    public String last_name;
    public String relationship_status;
    @Embedded public FbReference hometown;
    @Embedded public FbReference location;
    @Embedded public List<FbReference> languages;
    @Embedded public List<FbEducationExperience> education;
    @Embedded public List<FbWorkEntry> work;
}
