package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mongodb.morphia.annotations.Entity;

/**
 * Created by artem on 30.10.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public enum FbType {
    PAGE, USER
}
