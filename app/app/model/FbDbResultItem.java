package model;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Property;

/**
 * Created by volk on 04.12.15.
 */
@Entity("results")
public class FbDbResultItem {
    public ObjectId _id;
    @Property(value = "category")
    public String category;
    public Integer count;
    public String pages[];
}
