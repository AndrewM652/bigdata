package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by artem on 08.11.15
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FbLocation {
    public String city;
    public String country;
    public String name;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!(city == null))
            sb.append(city).append(", ");
        if (!(country == null))
            sb.append(country);
        return sb.toString();
    }
}
