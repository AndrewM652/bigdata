package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import model.FbExperience;
import model.FbReference;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import java.util.List;

/**
 * Created by andrewm on 02.11.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class FbEducationExperience {
    public String type;

    @Embedded public FbReference school;
    @Embedded public List<FbReference> concentration;
    @Embedded public FbReference degree;
    @Embedded public List<FbReference> with;
    @Embedded public List<FbExperience> classes;
    @Embedded public FbReference year;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!(school == null))
            sb.append("[Учебное заведение: ").append(school).append("; ");
        if (!(type == null))
            sb.append("Тип: ").append(type).append("; ");
        if (!(concentration == null))
            sb.append("Специализации: ").append(concentration).append("; ");
        if (!(degree == null))
            sb.append("Степень: ").append(degree).append("; ");
        if (!(with == null))
            sb.append("Вместе с: ").append(with).append("; ");
        if (!(classes == null))
            sb.append("Классы: ").append(classes).append("; ");
        if (!(year == null))
            sb.append("Год: ").append(year).append(".]");
        return sb.toString();
    }
}
