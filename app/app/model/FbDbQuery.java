package model;

/**
 * Created by andrewm on 15.11.15.
 */
public class FbDbQuery {

    public FbPage[] data;
    public FbDbPaging paging;

    public  FbSearchResponse toResponse() {
        FbSearchResponse response = new FbSearchResponse();
        response.data = data;
        FbPaging converted = new FbPaging();
        if(paging != null) {
            converted.next = paging.next;
            converted.previous = paging.previous;
        }
        response.paging = converted;
        return response;
    }
}
