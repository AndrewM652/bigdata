package model;

/**
 * Created by andrewm on 15.11.15.
 */

public class FbDbPaging {
    public String next;
    public String previous;

    public FbDbPaging(String next, String previous) {
        this.next = next;
        this.previous = previous;
    }
}
