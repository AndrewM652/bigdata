package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import model.FbReference;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.List;

/**
 * Created by andrewm on 02.11.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class FbProject {
    @Id public String id;
    public String name;
    public String description;
    public String startDate;
    public String endDate;
    @Embedded public FbReference from;
    @Embedded public List<FbReference> with;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!(name == null))
            sb.append("Проект: ").append(name).append("; ");
        if (!(description == null))
            sb.append("Описание: ").append(description).append("; ");
        if (!(startDate == null))
            sb.append("С ").append(startDate).append(" ");
        if (!(endDate == null))
            sb.append("по ").append(endDate).append("; ");
        if (!(from == null))
            sb.append("from: ").append(from).append("; ");
        if (!(with == null))
            sb.append("Вместе с ").append(with).append("; ");
        return sb.toString();
    }
}
