package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mongodb.morphia.annotations.Embedded;

import java.util.List;

/**
 * Created by andrewm on 02.11.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FbWorkEntry {
    public String startDate;
    public String endDate;

    @Embedded public FbReference employer;
    @Embedded public FbPage position;
    @Embedded public FbPage location;
    @Embedded public List<FbProject> projects;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!(employer == null))
            sb.append("Работодатель: ").append(employer).append("; ");
        if (!(position == null))
            sb.append("Должность: ").append(position).append("; ");
        if (!(startDate == null))
            sb.append("С ").append(startDate).append(" ");
        if (!(endDate == null))
            sb.append("по ").append(endDate).append("; ");
        if (!(location == null))
            sb.append("Город: ").append(location).append("; ");
        if (!(projects == null))
            sb.append("Проекты: ").append(projects).append("; ");
        return sb.toString();
    }
}
