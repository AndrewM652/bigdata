package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import model.FbReference;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.List;

/**
 * Created by andrewm on 02.11.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class FbExperience {
    @Id public String id;
    public String description;
    public String name;
    @Embedded public FbReference from;
    @Embedded public List<FbReference> with;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!(name == null))
            sb.append("Предмет: ").append(name).append("; ");
        if (!(description == null))
            sb.append("Описание: ").append(description).append("; ");
        if (!(from == null))
            sb.append("С ").append(from).append(" года; ");
        if (!(with == null))
            sb.append("Вместе с ").append(with).append("; ");
        return sb.toString();
    }
}
