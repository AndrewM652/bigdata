import akka.actor.ActorRef;
import akka.actor.Props;
import akka.aktors.LoggerActor;
import play.Application;
import play.GlobalSettings;
import play.libs.Akka;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

/**
 * Created by artem on 03.10.15.
 */
public class Global extends GlobalSettings {

    private static final int DELAY = 0 ;    //initial delay
    private static final int INTERVAL = 60; //between background work in seconds

    private ActorRef loggerActor;

    @Override
    public void onStart (Application app){
        loggerActor = Akka.system().actorOf(Props.create(LoggerActor.class));

        Akka.system()
                .scheduler()
                .schedule(
                        Duration.create(DELAY, TimeUnit.MILLISECONDS),
                        Duration.create(INTERVAL, TimeUnit.SECONDS),
                        () -> {
                            background();
                        },
                        Akka.system().dispatcher());
    }

    private void background(){
        loggerActor.tell("tick", ActorRef.noSender());
    }
}
