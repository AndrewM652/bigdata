package akka.aktors;

import akka.actor.UntypedActor;
import play.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by artem on 03.10.15.
 */
public class LoggerActor extends UntypedActor {

    private DateFormat formatter = new SimpleDateFormat("dd MMMM, hh:mm:ss");

    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof String) {
            Date date = new Date();
            Logger.debug(formatter.format(date) + " " + message);
            return;
        }

        unhandled(message);
    }

}
