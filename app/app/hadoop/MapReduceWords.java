package hadoop;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.hadoop.MongoInputFormat;
import com.mongodb.hadoop.MongoOutputFormat;
import com.mongodb.hadoop.io.BSONWritable;
import com.mongodb.hadoop.io.MongoUpdateWritable;
import com.mongodb.hadoop.util.MongoConfigUtil;
import com.mongodb.hadoop.util.MongoTool;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.ToolRunner;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.StringTokenizer;


/**
 * Created by hduser on 10/24/15.
 */
public class MapReduceWords extends MongoTool {
    public static String test_db = "facebook";
    public static String input_coll = "pages";
    public static String output_coll = "results";
    public static String MapObject;

    public static class AgregateMapper extends Mapper<Object, BSONObject, Text, Text>
    {
        private final Text keyText;
        private final Text valueText;

        public AgregateMapper() {
            super();
            keyText = new Text();
            valueText = new Text();
        }

        @Override
        public void map(final Object key, final BSONObject val, final Context context) throws IOException, InterruptedException {
            try {
                if(val.get(MapObject) instanceof String) {
                    String keyOutNotParsed = (String) val.get(MapObject);
                    StringTokenizer itr = new StringTokenizer(keyOutNotParsed, " \t\n\r\"/~!@#$%^&*()_+=-0987654321`,.?" +
                            "№;:'<>");
                    valueText.set("one");
                    while (itr.hasMoreTokens()) {
                        String result = itr.nextToken();
                        keyText.set(result.toLowerCase());
                        context.write(keyText, valueText);
                    }
                    System.out.println("MapReduceWords.map()::null -> " + val.get(MapObject));
                } else {
                    System.out.println("MapReduceWords.map()::null -> val.get(MapObject)");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public static class AgregateReducer extends Reducer<Text, Text, NullWritable, MongoUpdateWritable> {

        private MongoUpdateWritable reduceResult;

        public AgregateReducer() {
            super();
        }

        @Override
        public void reduce(final Text pKey, final Iterable<Text> pValues, final Context pContext) throws IOException, InterruptedException {
            BasicBSONObject query = new BasicBSONObject(MapObject, pKey.toString());
            Integer res=0;
            for (Text val : pValues) {
                res=res+1;
            }
            BasicBSONObject update=new BasicBSONObject();
            update.append("$set", new BasicBSONObject().append("count",res.toString()));
            reduceResult = new MongoUpdateWritable(query,update);
            pContext.write(null, reduceResult);
        }
    }

    public MapReduceWords(String MapObj) throws UnknownHostException {
        setConf(new Configuration());
        MongoConfigUtil.setInputFormat(getConf(), MongoInputFormat.class);
        MongoConfigUtil.setOutputFormat(getConf(), MongoOutputFormat.class);

        MongoConfigUtil.setInputURI(getConf(), "mongodb://localhost:27017/" + test_db + "." + input_coll);
        MongoConfigUtil.setOutputURI(getConf(), "mongodb://localhost:27017/" + test_db + "." + output_coll);

        MongoConfigUtil.setMapper(getConf(), AgregateMapper.class);
        MongoConfigUtil.setReducer(getConf(), AgregateReducer.class);
        MongoConfigUtil.setMapperOutputKey(getConf(), Text.class);
        MongoConfigUtil.setMapperOutputValue(getConf(), Text.class);
        MongoConfigUtil.setOutputKey(getConf(), IntWritable.class);
        MongoConfigUtil.setOutputValue(getConf(), BSONWritable.class);
        MapObject = MapObj;
    }

    public static void RunJobWords(final String[] pArgs, String MapObj) throws Exception {
        final MongoClient client = new MongoClient();
        DB db = client.getDB(test_db);
        DBCollection results = db.getCollection(output_coll);
        results.drop();
        ToolRunner.run(new MapReduceWords(MapObj), pArgs);
    }
}
