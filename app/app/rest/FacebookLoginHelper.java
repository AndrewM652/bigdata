package rest;

import com.squareup.okhttp.*;
import play.Logger;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Created by volk on 29.10.15.
 */
public class FacebookLoginHelper implements IFacebookLoginHelper {

    private static final String TAG = "FacebookLoginHelper";
    private static final String CLIENT_ID = "1481551498839932";
    private static final String CLIENT_SECRET = "92dd9ed0bf32d7e82a61f81667a068d2";
    private static final String GRANT_TYPE = "client_credentials";

    private static OkHttpClient okHttpClient = new OkHttpClient();
    private static String accessToken;
    private static String userToken;

    @Override
    public String getAppToken() {
        if (accessToken == null) {
            String result = null;
            Request request = new Request.Builder()
                    .get()
                    .url(getUrl())
                    .build();

            Call call = okHttpClient.newCall(request);
            try {
                Response response = call.execute();
                result = parseFromResponse(response);
            } catch (IOException e) {
                Logger.error(TAG + ": " + "cannot request!");
            }
             accessToken = result;
        }
        return  accessToken;
    }

    @Override
    public String getUserToken() {
        return userToken;
    }

    @Override
    public void setUserToken(String token) {
        if (token == null || token.isEmpty())
            userToken = null;
        else
            userToken = token;
    }

    private HttpUrl getUrl() {
        return new HttpUrl.Builder()
                .scheme("https")
                .host("graph.facebook.com")
                .addPathSegment("oauth")
                .addPathSegment("access_token")
                .addQueryParameter("client_id", CLIENT_ID)
                .addQueryParameter("client_secret", CLIENT_SECRET)
                .addQueryParameter("grant_type", GRANT_TYPE)
                .build();
    }

    private String parseFromResponse(Response response) throws IOException {
        ResponseBody body = response.body();
        ByteBuffer temp = ByteBuffer.wrap(body.bytes());
        String decoded = Charset.forName("utf-8").decode(temp).toString();
        String[] arr = decoded.split("access_token=");
        if (arr.length == 2) {
            return arr[1];
        } else {
            return null;
        }
    }

}
