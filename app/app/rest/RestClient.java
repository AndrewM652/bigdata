package rest;

import com.squareup.okhttp.OkHttpClient;
import retrofit.JacksonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by artem on 04.10.15.
 */
public class RestClient {

    private static volatile RestClient instance;
    private static IFacebookService facebookService;

    public static RestClient getInstance() {
        if(instance == null) restart();
        return instance;
    }

    public static IFacebookService getFacebookService(){ return getInstance().facebookService;}

    private static void restart(){
        instance = new RestClient();
    }

    private RestClient(){
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.interceptors().add(new FbInterceptor());

        Retrofit adapter =  new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(JacksonConverterFactory.create())
                .baseUrl(Constants.FACEBOOK_ENDPOINT)
                .build();

        facebookService = adapter.create(IFacebookService.class);
    }

}
