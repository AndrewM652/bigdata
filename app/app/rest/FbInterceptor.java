package rest;

import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.net.URLEncoder;

import static rest.Constants.*;

/**
 * Created by artem on 30.10.15
 */
public class FbInterceptor implements Interceptor{

    private static final String TAG = "FbInterceptor";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder result = request.newBuilder();
        HttpUrl url = request.httpUrl();
        IFacebookLoginHelper loginHelper = new FacebookLoginHelper();
        String accessToken = loginHelper.getAppToken();
        if (accessToken != null) {
            url = processUrl(url, accessToken);
        }
        result.url(url.newBuilder().addQueryParameter("debug", "all").build());
        return chain.proceed(result.build());
    }

    private HttpUrl processUrl(HttpUrl request, String token) throws IOException {
        token = URLEncoder.encode(token);
        if (token == null) {
            throw new IOException();
        } else {
            return request
                    .newBuilder()
                    .addEncodedQueryParameter("access_token", token)
                    .build();
        }
    }
}
