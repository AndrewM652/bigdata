package rest;

import com.mongodb.MongoClient;

/**
 * Created by volk on 11.11.15.
 */
public class MongoClientSingleton {
    private static volatile MongoClientSingleton instance;

    public MongoClient client;

    private MongoClientSingleton(){
        client = new MongoClient();
    }

    public static MongoClientSingleton getInstance(){
        if (instance == null)
            synchronized (MongoClientSingleton.class){
                if (instance==null)
                    instance = new MongoClientSingleton();
            }
        return instance;
    }
}
