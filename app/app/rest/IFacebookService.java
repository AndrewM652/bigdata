package rest;

import model.FbSearchResponse;
import model.FbType;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Query;
import retrofit.http.Url;

import static rest.Constants.*;

/**
 * Created by volk on 29.10.15.
 */
public interface IFacebookService {

    @GET("v2.5/search")
    Call<FbSearchResponse> search(@Query("q") String text, @Query("type") FbType searchType);

    @GET("v2.5/search")
    Call<FbSearchResponse> search(@Query("q") String text,
                                  @Query("type") FbType searchType,
                                  @Query("fields") String fields);

    @GET()
    Call<FbSearchResponse> search(@Url() String paging);


}
