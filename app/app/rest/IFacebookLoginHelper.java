package rest;

/**
 * Created by artem on 03.11.15
 */
public interface IFacebookLoginHelper {
    String getAppToken();
    String getUserToken();
    void setUserToken(String token);
}
